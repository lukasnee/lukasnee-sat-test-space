For good testing experience using VSCode - install these extensions:

NOTE. For keyboard insert button to work as expected
Name: Overtype
Id: adammaras.overtype
Description: Provides insert/overtype mode.
Version: 0.2.0
Publisher: Adam Maras
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=adammaras.overtype

Name: PowerShell
Id: ms-vscode.powershell
Description: (Preview) Develop PowerShell scripts in Visual Studio Code!
Version: 2020.6.0
Publisher: Microsoft
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=ms-vscode.PowerShell


NOTE. you will have to install python on your machine too.
Name: reStructuredText
Id: lextudio.restructuredtext
Description: reStructuredText language support (RST/ReST linter, preview, IntelliSense and more)
Version: 142.0.0
Publisher: LeXtudio Inc.
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=lextudio.restructuredtext

Name: Draw.io Integration
Id: hediet.vscode-drawio
Description: This unofficial extension integrates Draw.io into VS Code.
Version: 1.2.0
Publisher: Henning Dieterichs
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=hediet.vscode-drawio

Name: vscode-icons
Id: vscode-icons-team.vscode-icons
Description: Icons for Visual Studio Code
Version: 11.1.0
Publisher: VSCode Icons Team
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=vscode-icons-team.vscode-icons

Name: Zip File Explorer
Id: slevesque.vscode-zipexplorer
Description: Display the content of a Zip file in a Tree Explorer
Version: 0.3.1
Publisher: slevesque
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=slevesque.vscode-zipexplorer

Name: vscode-pdf
Id: tomoki1207.pdf
Description: Display pdf file in VSCode.
Version: 1.1.0
Publisher: tomoki1207
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=tomoki1207.pdf