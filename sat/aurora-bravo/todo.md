# STRUCTURE:
- [EPICS] 
    - [STORIES] 
        - [SUBTASKS/TESTS]

# TODO:

- [{sats} Aurora Bravo satellite testing (2020)](https://nanoavionics.atlassian.net/browse/TS-749)

    - [Testing after environmental tests](https://nanoavionics.atlassian.net/browse/TS-807)

        https://nanoavionics.atlassian.net/browse/TS-824 s-band: thermals: 15min of continuous Tx test
        https://nanoavionics.atlassian.net/browse/TS-822 sband telemetry check
        https://nanoavionics.atlassian.net/browse/TS-828 UHF thrmals check
        https://nanoavionics.atlassian.net/browse/TS-827 UHF communication check
        https://nanoavionics.atlassian.net/browse/TS-833 UHF encryption and ID test
        https://nanoavionics.atlassian.net/browse/TS-838 GPS time and PPS check





