
# ------------------------------------------------------------------------------
# Input
# ------------------------------------------------------------------------------
$clientRelativePath = $args[0]
$ip = $args[1]
$port = $args[2]
$descriptionMessage = $args[3]
# ------------------------------------------------------------------------------
# Script
# ------------------------------------------------------------------------------

$scriptName = $MyInvocation.MyCommand.Name

if (  $args[0] -eq $null )
{
    Write-Host "Usage:"
    Write-Host "  ./${scriptName} <nanoMCSexePath> <ip> <port> ""custom log message"""
    exit
}

# format logfile path and command
$timestamp = Get-Date -Format FileDateTime
$timestampPretty = Get-Date

$logPath = "${scriptName}_${timestamp}.log"
$command = "$PSScriptRoot\$clientRelativePath -c $ip $port --logfile $logPath"

Write-Host "executing: $command"

Add-Content $logPath "Description: $descriptionMessage"
Add-Content $logPath "Date: $timestampPretty"
Add-Content $logPath "Command: $command"
Add-Content $logPath ""

# execute
Invoke-Expression -Command $command

# ------------------------------------------------------------------------------
# EOF
# ------------------------------------------------------------------------------
