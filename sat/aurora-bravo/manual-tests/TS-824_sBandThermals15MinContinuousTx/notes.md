https://nanoavionics.atlassian.net/browse/TS-824 s-band: thermals: 15min of continuous Tx test

## REFERENCE
- already executed test example:
    `"Z:\Engineering\Satellite Projects\Aurora 6U\Tests_Bravo\4. Testing after environmental tests\24.TC-SAT-059 s-band thermals 15min of continuous Tx tes"`

## BEFORE TESTING
- Verify that SRS4 (main and redundant) tx power is good (33dBm) using srs4reader.
    - Over srs4reader do `sband list tm` (see for pout) if it's not desired, do `sband set pout 33`
use 'ft download 6 <big_file_id> –p 0 –r 0' // download arguments for tx stress test over sband

- On redundant sband test, enable according EPS output channels:
    - `pr load <eps_structure_file>`
    - `pr list` 
    > to see structures
    - `pr send 6`
    > Set_output_single_t_TX, type EPS Output channel and delay (0)