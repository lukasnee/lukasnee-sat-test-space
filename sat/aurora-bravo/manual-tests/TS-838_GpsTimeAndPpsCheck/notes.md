https://nanoavionics.atlassian.net/browse/TS-838 GPS time and PPS check

## REFERENCE
already executed example: `"z:\Engineering\Satellite Projects\Aurora 6U\Tests_Bravo\4. Testing after environmental tests\"`

## NOTES

- save FC config to file (to restore config later)

nanoMCS>help time
    - use `time getoffset <cspid>`
    - use `time getlastpps <cspid>`
To see number of PPS increasing in FC, see:
`nanoMCS>remotecli 3 -c "rtc getsyncstats"`

FC>help rtc
