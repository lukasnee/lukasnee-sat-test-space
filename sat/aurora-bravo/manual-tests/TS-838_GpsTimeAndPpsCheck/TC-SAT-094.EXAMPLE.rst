TC-SAT-094 GPS time and PPS check . . . PASS
======================================================================================================================

https://nanoavionics.atlassian.net/browse/TS-330

1. Test items
---------------------------------------------------------------------------------------------------------------------
- FC time, synced from GPS, time accuracy (maybe its not a GPS time); +-1sec is ok
- FC receives PPS signal


2. Preconditions, post-conditions
---------------------------------------------------------------------------------------------------------------------

2.1. Test preconditions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
- save FC config to file (to restore config later)
- satellite can sync with GPS

2.2. Test step preconditions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
None

2.3. Test step post-conditions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
None

2.4. Test postconditions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
- write summary - add PASS/FAIL, comment, FW versions, date when test was performed.
- restore initial config of FC


3. Test steps
---------------------------------------------------------------------------------------------------------------------

+----+--------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------+---------------------------------------------+-------+
| No.|Actions                                                                                                             | Expected result                                         | Actual result                               | PASS? |
+====+====================================================================================================================+=========================================================+=============================================+=======+
| 1. | **FC time match current time**                                                                                     | - time match (difference <1sec)                         | - times match (difference is ~  -0.063842s) |       |
+    +--------------------------------------------------------------------------------------------------------------------+                                                         |                                             |       |
|    | - reboot FC                                                                                                        |                                                         |                                             |       |
|    | - wait until GPS will sync time of FC                                                                              |                                                         |                                             | PASS  |
|    | - sync personal computer (PC) time with time sync server (e.g. time.nist.gov)                                      |                                                         |                                             |       |
|    | - compare personal computer (PC) and FC times (use nanoMCS command time getoffset)                                 |                                                         |                                             |       |
+----+--------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------+---------------------------------------------+-------+
| 2. | **Time offset check**                                                                                              | - FC time is 50sec ahead personal computer time         | - time is 50s ahead of personal computers   |       |
+    +--------------------------------------------------------------------------------------------------------------------+                                                         |                                             |       |
|    | - change GPS time offset value to value, bigger by 50 from current                                                 |                                                         |                                             | PASS  |
|    | - repeat step 1                                                                                                    |                                                         |                                             |       |
+----+--------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------+---------------------------------------------+-------+
| 3. | **FC receives PPS from GPS**                                                                                       | - "Number of received PPS is increasing 1 PPS per second| - Number of received PPS is increasing      |       |
+    +--------------------------------------------------------------------------------------------------------------------+                                                         |   at a rate of 1 PPS per second             | PASS  |
|    | - execute FC CLI command "rtc getsyncstats" few times                                                              |                                                         |                                             |       |
+----+--------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------+---------------------------------------------+-------+




4. Summary
---------------------------------------------------------------------------------------------------------------------

2020-12-15 Edvard

Test passed successfully, FC correctly syncs time from GPS, GPS time offset works correctly and PPS is increasing 1 PPS per second. 


FW used:

.. code-block:: text

   imageType: application
   mcuUid: 002D00483038511635323335
   versionOfBootloaderL1: 1.1.0
   versionOfBootloaderL2: 1.1.2
   versionOfApplication: 1.3.9
   BL1 repository name: m6p-fc-aur
   BL1 gitHash: E8DDD194AC5D02072E323E924824F2CCFB344424
   BL2 repository name: m6p-fc-aur
   BL2 gitHash: E8DDD194AC5D02072E323E924824F2CCFB344424
   APP repository name: m6p-fc-aur
   APP gitHash: E8DDD194AC5D02072E323E924824F2CCFB344424

