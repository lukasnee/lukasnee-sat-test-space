
https://nanoavionics.atlassian.net/browse/TS-822 sband telemetry check


## REFERENCE:
already executed test example: `"Z:\Engineering\Satellite Projects\Aurora 6U\Tests_Bravo\4. Testing after environmental tests\25.TC-SAT-061 s-band telemetry check"`

## BEFORE TESTING
- For aurora bravo having two SRS4, primary and secondary, this test has to be executed twice (duplicating test action tables is an option)
- Verify that SRS4 (main and redundant) tx power is good (33dBm) using srs4reader. Over srs4reader do `sband list tm` (see for pout) if it's not desired, do `sband set pout 33`
- In case of redudant SRS4, verify that EPS outputs are on for respective PC and SRS4 (use EPS protocol structure and `pr parse` to configure channels)
- In EPS GeneralTelemetry (read using nanoMCS and EPS protocol structure):
    - 18. vBatt 
    > 2S Li-Ion battery voltage in mV.
    - 51. outputStatus 
    > EPS channel outputs bitwise expresion (LSB - channel 1; ESP supports 18 channels in total)

- for sband firmware version do 'srs4reader>sband list sys'
