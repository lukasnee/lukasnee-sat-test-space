TC-SAT-061 s-band: telemetry check . . . ????[PASS/FAIL]
======================================================================================================================

https://nanoavionics.atlassian.net/browse/TS-301

1. Test items
---------------------------------------------------------------------------------------------------------------------
- SRS-4 telemetry

2. Preconditions, post-conditions
---------------------------------------------------------------------------------------------------------------------

2.1. Test preconditions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
None

2.2. Test step preconditions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
None

2.3. Test step post-conditions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
None

2.4. Test postconditions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Write summary - add PASS/FAIL, comment, FW versions, date when test was performed.

3. Test steps
---------------------------------------------------------------------------------------------------------------------

For Main SRS-4:

+----+--------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------+---------------------------------------------+-------+
| No.|Actions                                                                                                             | Expected result                                         | Actual result                               | PASS? |
+====+====================================================================================================================+=========================================================+=============================================+=======+
| 1. | **Tx stats**                                                                                                       | - forward power is similar to set power                 | - ?                                         | ????  |
+    +--------------------------------------------------------------------------------------------------------------------+ - reflected power is at least 10dB lower than forward   |                                             |       |
|    | - get sband Tx configuration page, communication is through s-band GS                                              |                                                         |                                             |       |
+----+--------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------+---------------------------------------------+-------+
| 2. | **Rx stats**                                                                                                       | Measured RSSI is close to expected.                     | - ?                                         | ????  |
+    +--------------------------------------------------------------------------------------------------------------------+ Expected RSSI depends on various conditions and is      |                                             |       |
|    | - get sband Rx configuration page, communication is through s-band GS                                              | satellite specific                                      |                                             |       |
+----+--------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------+---------------------------------------------+-------+
| 3. | **tm stats**                                                                                                       | - temperatures are similar and are in 25-35C range      | - ?                                         | ????  |
+    +--------------------------------------------------------------------------------------------------------------------+ - input voltage is similar to current Vbat voltage      |                                             |       |
|    | - get sband tm configuration page, communication is through s-band GS                                              | - 3.75V, 3.3V, other voltages are +-0.15V               |                                             |       |
|    | - by using nanoMCS get Vbat voltage from EPS telemetries                                                           | - PA voltage is +-0.5V (should be 28.0V)                |                                             |       |
+----+--------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------+---------------------------------------------+-------+
| 4. | **tm stats** (local CAN)                                                                                           | - temperatures are similar and are in 25-35C range      | - ?                                         | ????  |
+    +--------------------------------------------------------------------------------------------------------------------+ - input voltage is similar to current Vbat voltage      |                                             |       |
|    | - get sband tm configuration page, communication is through local CAN                                              | - 3.75V, 3.3V voltages are +-0.15V                      |                                             |       |
|    | - take Vbat from previous step or read it again                                                                    | - currents are ~150mA on idle                           |                                             |       |
+----+--------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------+---------------------------------------------+-------+

For Redundant SRS-4:

+----+--------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------+---------------------------------------------+-------+
| No.|Actions                                                                                                             | Expected result                                         | Actual result                               | PASS? |
+====+====================================================================================================================+=========================================================+=============================================+=======+
| 1. | **Tx stats**                                                                                                       | - forward power is similar to set power                 | - ?                                         | ????  |
+    +--------------------------------------------------------------------------------------------------------------------+ - reflected power is at least 10dB lower than forward   |                                             |       |
|    | - get sband Tx configuration page, communication is through s-band GS                                              |                                                         |                                             |       |
+----+--------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------+---------------------------------------------+-------+
| 2. | **Rx stats**                                                                                                       | Measured RSSI is close to expected.                     | - ?                                         | ????  |
+    +--------------------------------------------------------------------------------------------------------------------+ Expected RSSI depends on various conditions and is      |                                             |       |
|    | - get sband Rx configuration page, communication is through s-band GS                                              | satellite specific                                      |                                             |       |
+----+--------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------+---------------------------------------------+-------+
| 3. | **tm stats**                                                                                                       | - temperatures are similar and are in 25-35C range      | - ?                                         | ????  |
+    +--------------------------------------------------------------------------------------------------------------------+ - input voltage is similar to current Vbat voltage      |                                             |       |
|    | - get sband tm configuration page, communication is through s-band GS                                              | - 3.75V, 3.3V, other voltages are +-0.15V               |                                             |       |
|    | - by using nanoMCS get Vbat voltage from EPS telemetries                                                           | - PA voltage is +-0.5V (should be 28.0V)                |                                             |       |
+----+--------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------+---------------------------------------------+-------+
| 4. | **tm stats** (local CAN)                                                                                           | - temperatures are similar and are in 25-35C range      | - ?                                         | ????  |
+    +--------------------------------------------------------------------------------------------------------------------+ - input voltage is similar to current Vbat voltage      |                                             |       |
|    | - get sband tm configuration page, communication is through local CAN                                              | - 3.75V, 3.3V voltages are +-0.15V                      |                                             |       |
|    | - take Vbat from previous step or read it again                                                                    | - currents are ~150mA on idle                           |                                             |       |
+----+--------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------+---------------------------------------------+-------+

To get temperatures of SRS4 Main:
- run srs4reader, communication is through s-band GS
- execute "idset 24" on srs4reader 
- execute "sband list tm" on srs4reader; same for tx, rx telemetries.

To get temperatures of SRS4 Redundant:
- execute "idset 23" on srs4reader 
- execute "setdst 9" on srs4reader
- execute "sband list tm" on srs4reader; same for tx, rx telemetries.

4. Summary
---------------------------------------------------------------------------------------------------------------------

 ???

5. Appendix
---------------------------------------------------------------------------------------------------------------------

 ???[log all read telemetries here, use ".. code-block:: text" to add block of code]

 