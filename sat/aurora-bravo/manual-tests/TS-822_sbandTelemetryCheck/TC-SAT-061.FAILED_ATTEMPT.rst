TC-SAT-061 s-band: telemetry check
======================================================================================================================

https://nanoavionics.atlassian.net/browse/TS-301

1. Test items
---------------------------------------------------------------------------------------------------------------------
- SRS4 telemetry

2. Preconditions, post-conditions
---------------------------------------------------------------------------------------------------------------------

2.1. Test preconditions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
None

2.2. Test step preconditions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
None

2.3. Test step post-conditions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
None

2.4. Test postconditions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Write summary - add PASS/FAIL, comment, FW versions, date when test was performed.

3. Test steps
---------------------------------------------------------------------------------------------------------------------

+----+--------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------+---------------------------------------------+-------+
| No.|Actions                                                                                                             | Expected result                                         | Actual result                               | PASS? |
+====+====================================================================================================================+=========================================================+=============================================+=======+
| 1. | **Tx stats**                                                                                                       | - forward power is similar to set power                 | - forward power: 33.0 dBm                   |       |
+    +--------------------------------------------------------------------------------------------------------------------+ - reflected power is at least 10dB lower than forward   | - set power: 33.0 dBm                       | PASS  |
|    | - Read SRS-4 main sband tx configuration page, communication is through s-band GS                                  |                                                         | - reflected power: 17.4 dBm                 |       |
+----+--------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------+---------------------------------------------+-------+
| 2. | **Rx stats**                                                                                                       | Measured RSSI is close to expected.                     | - measured RSSI: -80.5 dBm                  |       |
+    +--------------------------------------------------------------------------------------------------------------------+ Expected RSSI depends on various conditions and is      |                                             | PASS  |
|    | - get SRS-4 main sband Rx configuration page, communication is through s-band GS                                   | satellite specific                                      |                                             |       |
+----+--------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------+---------------------------------------------+-------+
| 3. | **tm stats**                                                                                                       | - temperatures are similar and are in 25-35C range      | - temperatures from 30 to 34 deg C          |       |
+    +--------------------------------------------------------------------------------------------------------------------+ - input voltage is similar to current Vbat voltage      | - vin: 7133 mV                              |       |
|    | - get SRS-4 main sband tm configuration page, communication is through s-band GS                                   | - other voltages are +-5%                               | - vBatt: 7223 mV                            |       |
|    | - by using nanoMCS get Vbat voltage from EPS telemetries                                                           |                                                         | - 3v3: 3360 mV                              |       |
|    |                                                                                                                    |                                                         | - 1v8: 1803 mV                              | PASS  |
|    |                                                                                                                    |                                                         | - 1v0: 965 mV                               |       |
|    |                                                                                                                    |                                                         | - pa (28V): 27713 mV                        |       |
+----+--------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------+---------------------------------------------+-------+
| 4. | **tm stats** (local CAN)                                                                                           | - temperatures are similar and are in 25-35C range      | - temperatures from 28 to 29 deg C          |       |
+    +--------------------------------------------------------------------------------------------------------------------+ - input voltage is similar to current Vbat voltage      | - vBat 7223 mV                              |       |
|    | - repeat step 3 on SRS-4 main through local CAN                                                                    | - other voltages are +-5%                               | - 3V3: 3353 mV                              | PASS  |
|    |                                                                                                                    |                                                         | - 1V8: 1798 mV                              |       |
|    |                                                                                                                    |                                                         | - 1V0: 965 mV                               |       |
|    |                                                                                                                    |                                                         | - PA (28V): 5 mV                            |       |
|    |                                                                                                                    |                                                         | - VIN: 7285 mV                              |       |
+----+--------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------+---------------------------------------------+-------+
| 5. | **redundand SRS-4 test**                                                                                           | - same as steps 1-4                                     | - forward power: 32.93 dBm                  |       |
+    +--------------------------------------------------------------------------------------------------------------------+                                                         | - set power: 33.0 dBm                       |       |
|    | - repeat steps 1-4 on redundant SRS-4 and redundant PC                                                             |                                                         | - reflected power: 2.85 dBm                 |       |
|    |                                                                                                                    |                                                         |---------------------------------------------|       |
|    |                                                                                                                    |                                                         | - measured RSSI: -66.3 dBm                  |       |
|    |                                                                                                                    |                                                         |                                             |       |
|    |                                                                                                                    |                                                         |                                             |       |
|    |                                                                                                                    |                                                         |---------------------------------------------|       |
|    |                                                                                                                    |                                                         | - temperatures from 29 to 33 deg C          |       |
|    |                                                                                                                    |                                                         | - vin: 7132 mV                              |       |
|    |                                                                                                                    |                                                         | - vBatt: 7231 mV                            |       |
|    |                                                                                                                    |                                                         | - 3v3: 3365 mV                              | PASS  |
|    |                                                                                                                    |                                                         | - 1v8: 1808 mV                              |       |
|    |                                                                                                                    |                                                         | - 1v0: 961 mV                               |       |
|    |                                                                                                                    |                                                         | - pa (28V): 27616 mV                        |       |
|    |                                                                                                                    |                                                         |---------------------------------------------|       |
|    |                                                                                                                    |                                                         | - temperatures from 29 to 33 deg C          |       |
|    |                                                                                                                    |                                                         | - vBat 7231 mV                              |       |
|    |                                                                                                                    |                                                         | - 3V3: 3358 mV                              |       |
|    |                                                                                                                    |                                                         | - 1V8: 1805 mV                              |       |
|    |                                                                                                                    |                                                         | - 1V0: 961 mV                               |       |
|    |                                                                                                                    |                                                         | - PA (28V): 3 mV                            |       |
|    |                                                                                                                    |                                                         | - VIN: 7282 mV                              |       |
+----+--------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------+---------------------------------------------+-------+

To get telemetries of SRS4:
- run srs4reader, communication is through s-band GS or local can
- execute "idset 24" on srs4reader for SRS-4 main s-band GS case
- execute "idset 23" and "sband setdst 9" on srs4reader for SRS-4 redundant s-band GS case
- execute "sband prop tx" on srs4reader to get Tx telemetry; same for other telemetries

4. Summary
---------------------------------------------------------------------------------------------------------------------
Note measured RSSI test over redudant SRS-4 s-band - it is lower 24 dbm higher than RSSI measurement over main s-band.
Note PA measurement over CAN on both main and redudant s-band tests - appears to be off (expected 28 V, but actualy is a few mV). 

firmware versions:

PC (redudant): 
versionOfBootloaderL1: 0.0.9
versionOfBootloaderL2: 0.0.18
versionOfApplication: 0.0.25
BL1 repository name: m6p-pc-v1.5
BL1 gitHash: 2859526CFC4AA561C6AEA8E4C75A164862FC95C9
BL2 repository name: m6p-pc-v1.5
BL2 gitHash: 2859526CFC4AA561C6AEA8E4C75A164862FC95C9
APP repository name: m6p-pc-v1.5
APP gitHash: 2859526CFC4AA561C6AEA8E4C75A164862FC95C9

PC (main):
versionOfBootloaderL1: 0.0.9
versionOfBootloaderL2: 0.0.18
versionOfApplication: 0.0.25
BL1 repository name: m6p-pc-v1.5
BL1 gitHash: 2859526CFC4AA561C6AEA8E4C75A164862FC95C9
BL2 repository name: m6p-pc-v1.5
BL2 gitHash: 2859526CFC4AA561C6AEA8E4C75A164862FC95C9
APP repository name: m6p-pc-v1.5
APP gitHash: 2859526CFC4AA561C6AEA8E4C75A164862FC95C9

S-Band (main):
sw.version      u32   01010000

S-Band (redundant):
sw.version      u32   01010000
-

Test date: 2020-12-18

Test - PASS.
Test executed by Lukas Neverauskis.

