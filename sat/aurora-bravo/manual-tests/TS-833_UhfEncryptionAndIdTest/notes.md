
https://nanoavionics.atlassian.net/browse/TS-833 UHF encryption and ID test

## REFERENCE 
already executed example: 
`"Z:\Engineering\Satellite Projects\Aurora 6U\Tests_Bravo\4. Testing after environmental tests\15. TC-SAT-035 UHF encryption and ID test"`

## BEFORE TESTING
Test with good encryption key and radio ID, bad, etc.

the general idea is that communication between GS and SAT COMMs is viable only if `radioAddr` are matching, encryption is both disabled or enabled having exactly the same encryption keys and initial vectors IV.
Manipulate listed variables encryption to see if communication behaves as expected.

BACKUP CONFIG BEFORE EDITING!

Write SUMMARY

## NOTES. 
- verify communication by pinging multiple times
- for SAT COMM use pr parse COMM structure TXOn
- for GS use `remotecli 16 -c “radio txon”`
- edit all encryption variables using `configc` in nanoMCS: import full config as template, `get` and `export` as yaml, edit yaml's encryption variables (delete everything else) to make diff file, then `import` and `set`.

