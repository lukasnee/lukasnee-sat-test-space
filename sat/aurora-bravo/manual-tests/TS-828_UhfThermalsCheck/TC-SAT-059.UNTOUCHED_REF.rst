TC-SAT-059 s-band: thermals: 15min of continuous Tx test
======================================================================================================================

https://nanoavionics.atlassian.net/browse/TS-217

Execution of this test can be combined with tests, listed below:
- TC-SAT-075 https://nanoavionics.atlassian.net/browse/TS-316 
- TC-SAT-076 https://nanoavionics.atlassian.net/browse/TS-317
- TC-SAT-031 https://nanoavionics.atlassian.net/browse/TS-235
- TC-SAT-044 https://nanoavionics.atlassian.net/browse/TS-208
- TC-SAT-034 https://nanoavionics.atlassian.net/browse/TS-238
- TC-SAT-059 https://nanoavionics.atlassian.net/browse/TS-217

1. Test items
---------------------------------------------------------------------------------------------------------------------
- SRS3 doesn't overheats per satellite pass (but test is in atmosphere)

2. Preconditions, post-conditions
---------------------------------------------------------------------------------------------------------------------

2.1. Test preconditions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
nanoMCS cfile to download files from any subsystems for ~15minutes with 1ms period

2.2. Test step preconditions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
None

2.3. Test step post-conditions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
None

2.4. Test postconditions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Write summary - add PASS/FAIL, comment, FW versions, date when test was performed.

3. Test steps
---------------------------------------------------------------------------------------------------------------------

+----+--------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------+---------------------------------------------+-------+
| No.|Actions                                                                                                             | Expected result                                         | Actual result                               | PASS? |
+====+====================================================================================================================+=========================================================+=============================================+=======+
| 1. | **Initial temperatures**                                                                                           | - initial temperatures are in range 25-35C              |                                             |       |
+    +--------------------------------------------------------------------------------------------------------------------+                                                         |                                             |       |
|    | - get initial temperatures of SRS3                                                                                 |                                                         |                                             |       |
|    | - add them to this document                                                                                        |                                                         |                                             |       |
+----+--------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------+---------------------------------------------+-------+
| 2. | **Temperatures after 15minutes of transmission**                                                                   | - temperatures are <50C                                 |                                             |       |
+    +--------------------------------------------------------------------------------------------------------------------+                                                         |                                             |       |
|    | - run nanoMCS with download script; execution should take ~15 minutes                                              |                                                         |                                             |       |
|    | - get final temperatures of SRS3                                                                                   |                                                         |                                             |       |
|    | - add them to this document                                                                                        |                                                         |                                             |       |
+----+--------------------------------------------------------------------------------------------------------------------+---------------------------------------------------------+---------------------------------------------+-------+

To get temperatures of SRS3:
- run srs3reader, communication is through s-band GS
- execute "idset 24" on srs3reader 
- execute "sband prop tm" on srs3reader

4. Summary
---------------------------------------------------------------------------------------------------------------------
