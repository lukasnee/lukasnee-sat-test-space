
https://nanoavionics.atlassian.net/browse/TS-828 UHF thrmals check

## REFERENCE 
already executed example: 
`Z:\Engineering\Satellite Projects\Aurora 6U\Tests_Bravo\4. Testing after environmental tests\17. TC-SAT-034 UHF thermals check (15min continuous Tx)`

## BEFORE TESTING
- Verify communication, set client as UHF ground csp id, set dst 1, turn on tx using comm protocol structure - try ping. 
- get temperature over COMM pr structure Get_GeneralTelemetry_TX, see values:
    - `powerAmplifierTemperature` in Celcius.
    - `quartzTemperature` in Celcius.

- Download big file on repeat:
    - `ft download 6 33 –p 1000 –r 0` 
    > download arguments for tx stress test over UHF: 1 sec period because UHF is slow and is prone to get stuck...

    - SAT COMM expected CSP ID 1.
    - GS COMM expected CSP ID 16.
    > use: nanoMCS>csp reboot <1 or 16> // in case problems
