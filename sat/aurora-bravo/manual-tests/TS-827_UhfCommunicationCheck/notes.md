
https://nanoavionics.atlassian.net/browse/TS-827 UHF communication check

## REFERENCE
Already executed test example: 
`"Z:\Engineering\Satellite Projects\Aurora 6U\Tests_Bravo\4. Testing after environmental tests\16. TC-SAT-030 UHF communication check"`
## BEFORE TESTING

- write your own .rst, see reference...

- Assure communication over UHF, SAT COMM tx power, maybe RSSI (log COMM telemetry, both on GS and SAT)

- Upload random file (~10kB) file with:
    - `ft upload 1 <file id, probably 5> uploaded.dat`

- Download same file, compare if it is bit perfect and validate that file trasfer ran without problems.
    - `ft download 1 <file id, probably 5> -p 1000` 
    >rename downloaded content to downloaded.dat

>Ask Vaidas for more details.