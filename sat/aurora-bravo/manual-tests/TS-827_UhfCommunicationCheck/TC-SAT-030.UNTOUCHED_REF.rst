TC-SAT-0xx UHF: communication check . . . PASS
======================================================================================================================

https://nanoavionics.atlassian.net/browse/TS-525

1. Summary
---------------------------------------------------------------------------------------------------------------------

2020-12-21 Edvard

Test passed: communication over UHF works as expected: file download and upload is successful.

:download:`Uploaded file<uploaded.dat>`

:download:`Downloaded file<downloaded.dat>`

:download:`Upload/Download log<log.txt>`

Note: Sat COMM tx power was set to 0 during this test.

FW COMM:

.. code-block:: text

   ----------------------------------------------------------
   CSP ID: 1
   ----------------------------------------------------------
   imageType: application
   mcuUid: 00360064374B501420353243
   versionOfBootloaderL1: 0.3.2
   versionOfBootloaderL2: 0.4.6
   versionOfApplication: 2.4.14
   BL1 repository name: comm
   BL1 gitHash: 11F8F1A2A5B8B145B5A037CBAB8D246F1F28615E
   BL2 repository name: comm
   BL2 gitHash: 11F8F1A2A5B8B145B5A037CBAB8D246F1F28615E
   APP repository name: comm
   APP gitHash: 11F8F1A2A5B8B145B5A037CBAB8D246F1F28615E


FW GS:

.. code-block:: text

   ----------------------------------------------------------
   CSP ID: 26
   ----------------------------------------------------------
   imageType: application
   mcuUid: 00660067374B501420353243
   versionOfBootloaderL1: 0.3.2
   versionOfBootloaderL2: 0.4.6
   versionOfApplication: 2.4.14
   BL1 repository name: comm
   BL1 gitHash: 11F8F1A2A5B8B145B5A037CBAB8D246F1F28615E
   BL2 repository name: comm
   BL2 gitHash: 11F8F1A2A5B8B145B5A037CBAB8D246F1F28615E
   APP repository name: comm
   APP gitHash: 11F8F1A2A5B8B145B5A037CBAB8D246F1F28615E